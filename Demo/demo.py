from face_detection_server import FaceDetectionServer
from pathlib import Path
import time
import sys

config = Path(__file__).parent / "config.yaml"
fd = FaceDetectionServer(config_path=config)
fd.start()

while True:
    try:
        time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        print("stopping")
        fd.stop()
        sys.exit()