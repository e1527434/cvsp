package facedetection.detectors;

import org.opencv.core.Mat;

import java.util.ArrayList;

public abstract class BenchmarkDetector implements Detector {

    protected ArrayList<Integer> benchmarks;
    protected long timeDiff, timeStart, timeEnd;
    boolean recording = false;

    protected abstract Mat detectFaces(Mat rgba, Mat gray);

    @Override
    public Mat detect(Mat rgba, Mat gray) {
        timeStart = System.currentTimeMillis();
        Mat result = detectFaces(rgba, gray);
        timeEnd = System.currentTimeMillis();
        timeDiff = timeEnd - timeStart;
        if (recording) {
            benchmarks.add((int) timeDiff);
        }
        return result;
    }

    public long getTimeDiff() {
        return timeDiff;
    }

    public float getFPS() {
        return 1 / (((float) timeDiff) / 1000.0f);
    }

    public void startRecording() {
        benchmarks = new ArrayList<>();
        recording = true;
    }

    public void stopRecording() {
        recording = false;
    }

    public boolean isRecording() {
        return recording;
    }

    public Iterable<Integer> getBanchmarks() {
        return benchmarks;
    }
}
