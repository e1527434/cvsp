package facedetection.detectors;

import org.opencv.core.Mat;
import org.opencv.core.Scalar;

public interface Detector {
    Scalar FACE_RECT_COLOR = new Scalar(255, 255, 0, 255);
    Mat detect(Mat rgba, Mat gray);
    long getTimeDiff();
    float getFPS();
    String getName();
}
