package facedetection.detectors;

import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;

public class JavaDetector extends BenchmarkDetector {

    public float mRelativeFaceSize = 0.2f;
    public int mAbsoluteFaceSize = 0;

    private CascadeClassifier mJavaDetector;

    public JavaDetector(File cascadeFile) {
        mJavaDetector = new CascadeClassifier(cascadeFile.getAbsolutePath());
        if (mJavaDetector.empty()) {
            Log.e(getName(), "Failed to load cascade classifier");
            mJavaDetector = null;
        } else
            Log.i(getName(), "Loaded cascade classifier from " + cascadeFile.getAbsolutePath());
    }

    @Override
    protected Mat detectFaces(Mat rgba, Mat gray) {
        if (mAbsoluteFaceSize == 0) {
            int height = gray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }

        if (mJavaDetector == null) return rgba;

        MatOfRect faces = new MatOfRect();
        mJavaDetector.detectMultiScale(gray, faces, 1.1, 2, 2, new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());

        Rect[] facesArray = faces.toArray();
        for (Rect rect : facesArray) {
            Imgproc.rectangle(rgba, rect.tl(), rect.br(), FACE_RECT_COLOR, 3);
        }

        return rgba;
    }

    @Override
    public String getName() {
        return "Java Detector";
    }
}
