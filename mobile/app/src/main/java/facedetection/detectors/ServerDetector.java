package facedetection.detectors;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.nio.ByteBuffer;

import facedetection.ServerCommunication;

public class ServerDetector extends BenchmarkDetector {

    private ServerCommunication comm;
    private Long serverStartTime = null;

    public void startCommunication(String host, int port) {
        comm = new ServerCommunication(host, port);
        comm.start();
    }

    public void stopCommunication() {
        if (comm == null) return;
        comm.interrupt();
        comm = null;
    }

    @Override
    protected Mat detectFaces(Mat rgba, Mat gray) {
        if (comm == null) return rgba;

        final int IN_WIDTH = 300;
        final int IN_HEIGHT = 169;

        Mat scaled = new Mat(new int[]{IN_HEIGHT, IN_WIDTH, 3}, CvType.CV_8U);
        Imgproc.resize(rgba, scaled, scaled.size());

        int cols = rgba.cols();
        int rows = rgba.rows();

        synchronized (comm.sync) {
            if (comm.rectangles != null) {
                for (int i = 0; i < comm.rectangles.length; ) {
                    Imgproc.rectangle(
                            rgba,
                            new Point(comm.rectangles[i++]*cols, comm.rectangles[i++]*rows),
                            new Point(comm.rectangles[i++]*cols, comm.rectangles[i++]*rows),
                            FACE_RECT_COLOR,
                            3
                    );
                }
                serverStartTime = comm.timeStamp;
            } else {
                serverStartTime = null;
            }
        }

        Imgproc.cvtColor(scaled, scaled, Imgproc.COLOR_RGB2BGR);

        MatOfByte buf = new MatOfByte();
        Imgcodecs.imencode(".jpg", scaled, buf);
        byte[] data = buf.toArray();
        byte[] result = ByteBuffer
                .allocate(1 + 8 + data.length)
                .put(ServerCommunication.OP_IMAGE)
                .putLong(super.timeStart) // timestamp
                .put(data)
                .array();
        comm.messageWithOp(result);
        buf.release();
        scaled.release();
        return rgba;
    }

    @Override
    public String getName() {
        return "Server Detector";
    }
}
