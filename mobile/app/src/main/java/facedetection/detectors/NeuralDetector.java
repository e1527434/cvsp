package facedetection.detectors;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgproc.Imgproc;
import org.opencv.samples.facedetect.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class NeuralDetector extends BenchmarkDetector {

    private Net net;

    public NeuralDetector(Context context) {
        String proto = getPath(context.getString(R.string.net_proto), context);
        String weights = getPath(context.getString(R.string.net_weights), context);
        net = Dnn.readNetFromCaffe(proto, weights);
    }

    // Upload file to storage and return a path.
    private static String getPath(String file, Context context) {
        AssetManager assetManager = context.getAssets();

        BufferedInputStream inputStream = null;
        try {
            // Read data from assets.
            inputStream = new BufferedInputStream(assetManager.open(file));
            byte[] data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();

            // Create copy file in storage.
            File outFile = new File(context.getFilesDir(), file);
            FileOutputStream os = new FileOutputStream(outFile);
            os.write(data);
            os.close();
            // Return a path to file which may be read in common way.
            return outFile.getAbsolutePath();
        } catch (IOException ex) {
            Log.e("FaceDetection", "Failed to upload a file");
        }
        return "";
    }

    @Override
    protected Mat detectFaces(Mat rgba, Mat gray) {
        final int IN_WIDTH = 300;
        final int IN_HEIGHT = 300;
        final double THRESHOLD = 0.2;

        Imgproc.cvtColor(rgba, rgba, Imgproc.COLOR_RGBA2RGB);
        Mat blob = Dnn.blobFromImage(rgba, 3, new Size(IN_WIDTH, IN_HEIGHT), new Scalar(104, 177, 123), true);
        net.setInput(blob);
        Mat detections = net.forward();
        blob.release();

        int cols = rgba.cols();
        int rows = rgba.rows();

        detections = detections.reshape(1, (int)detections.total() / 7);

        for (int i = 0; i < detections.rows(); ++i) {
            double confidence = detections.get(i, 2)[0];

            if (confidence > THRESHOLD) {
                int classId = (int)detections.get(i, 1)[0];

                int left   = (int)(detections.get(i, 3)[0] * cols);
                int top    = (int)(detections.get(i, 4)[0] * rows);
                int right  = (int)(detections.get(i, 5)[0] * cols);
                int bottom = (int)(detections.get(i, 6)[0] * rows);

                // Draw rectangle around detected object.
                Imgproc.rectangle(rgba, new Point(left, top), new Point(right, bottom), FACE_RECT_COLOR, 3);
            }
        }
        detections.release();
        return rgba;
    }

    @Override
    public String getName() {
        return "Neural Detector";
    }
}
