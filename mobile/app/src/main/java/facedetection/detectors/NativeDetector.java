package facedetection.detectors;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.io.File;

import facedetection.DetectionBasedTracker;

public class NativeDetector extends BenchmarkDetector {

    public float mRelativeFaceSize = 0.2f;
    public int mAbsoluteFaceSize = 0;

    private DetectionBasedTracker nativeDetector;

    public NativeDetector(File cascadeFile) {
        nativeDetector = new DetectionBasedTracker(cascadeFile.getAbsolutePath(), 0);
    }

    public void start() {
        nativeDetector.start();
    }

    public void stop() {
        nativeDetector.stop();
    }

    @Override
    protected Mat detectFaces(Mat rgba, Mat gray) {
        if (mAbsoluteFaceSize == 0) {
            int height = gray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
            nativeDetector.setMinFaceSize(mAbsoluteFaceSize);
        }

        if (nativeDetector == null) return rgba;

        MatOfRect faces = new MatOfRect();
        nativeDetector.detect(gray, faces);

        Rect[] facesArray = faces.toArray();
        for (Rect rect : facesArray) {
            Imgproc.rectangle(rgba, rect.tl(), rect.br(), FACE_RECT_COLOR, 3);
        }

        return rgba;
    }

    @Override
    public String getName() {
        return "Native Detector";
    }
}
