package facedetection;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ServerCommunication extends Thread {

    public static byte OP_UNKNOWN = 0;
    public static byte OP_FPS = 1;
    public static byte OP_IMAGE = 2;
    public static byte OP_TEXT = 3;

    private DatagramSocket socket;
    private String serverHostname;
    private int serverPort;
    private SocketAddress serverAddress;

    private static String TAG = "SERVER_COMMUNICATION";

    ConcurrentLinkedQueue<byte[]> queue = new ConcurrentLinkedQueue<>();
    public float[] rectangles;
    public long timeStamp;
    public final Object sync = new Object();

    public ServerCommunication(String host, int port) {
        serverHostname = host;
        serverPort = port;
    }

    public void messageWithOp(byte[] msg) {
        message(msg);
    }

    private void message(byte[] msg) {
        queue.add(msg);
    }

    public void message(float msg) {
        message(float2ByteArray(OP_FPS, msg));
    }

    public void message(byte op, float msg) {
        message(float2ByteArray(op, msg));
    }

    public void message(String msg) {
        message(OP_TEXT, msg);
    }

    public void message(byte op, String msg) {
        byte[] msgData = msg.getBytes();
        message(ByteBuffer.allocate(1 + msgData.length).put(op).put(msgData).array());
    }

    public void run(){
        try {
            socket = new DatagramSocket();
            socket.setSendBufferSize(40000);
            socket.setSoTimeout(100);
            serverAddress = new InetSocketAddress(serverHostname, serverPort);
            message("Hello Server");
            Log.i(TAG, "sent datagram");
        } catch (SocketException e) {
            String msg = e.getMessage();
            Log.e(TAG, "datagram server error (at creation): " + (msg == null ? "error when creating socket" : msg));
        }

        byte[] receiveBuffer = new byte[200*4*4+8]; // max 200 float (4 byte) rectangles specified by 4 values

        while(!this.isInterrupted()){
            byte[] msg;
            while ((msg = queue.poll()) != null) {
                // process msg
                try {
                    sendMessage(msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (msg[0] == OP_IMAGE) {
                    DatagramPacket response = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                    try {
                        socket.receive(response);
                        if (response.getLength() > 0) {
                            displayResponse(receiveBuffer, response.getLength());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                queue.clear();


            }
        }
    }

    private void displayResponse(byte[] receiveBuffer, int length) {
        float[] vals = new float[(length - 8) / 4];
        ByteBuffer.wrap(receiveBuffer, 8, vals.length*4).order(ByteOrder.LITTLE_ENDIAN).asFloatBuffer().get(vals);
        long timeStamp = ByteBuffer.wrap(receiveBuffer, 0, 8).getLong();
        synchronized (sync) {
            rectangles = vals;
            this.timeStamp = timeStamp;
        }
    }

    private void sendMessage(byte[] msg) throws IOException {
        socket.send(new DatagramPacket(msg,0,msg.length, serverAddress));
    }

    public static byte [] float2ByteArray (byte op, float value) {
        return ByteBuffer.allocate(5).put(op).putFloat(value).array();
    }
}
