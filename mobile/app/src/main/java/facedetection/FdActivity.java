package facedetection;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraActivity;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.samples.facedetect.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import facedetection.detectors.BenchmarkDetector;
import facedetection.detectors.JavaDetector;
import facedetection.detectors.NativeDetector;
import facedetection.detectors.NeuralDetector;
import facedetection.detectors.ServerDetector;

public class FdActivity extends CameraActivity implements CvCameraViewListener2 {

    private static final String TAG = "OCVSample::Activity";

    private MenuItem mItemJava;
    private MenuItem mItemNative;
    private MenuItem mItemNN;
    private MenuItem mItemNNServer;
    private MenuItem mItemBenchmarks;

    private MenuItem mCurrentDetector;
    private boolean printing = false;

    private TextView fpsText;

    private Mat mRgba;
    private Mat mGray;

    private CameraBridgeViewBase mOpenCvCameraView;

    private JavaDetector javaDetector;
    private NativeDetector nativeDetector;
    private NeuralDetector neuralDetector;
    private ServerDetector serverDetector;
    private BenchmarkDetector currentDetector;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            if (status == LoaderCallbackInterface.SUCCESS) {
                Log.i(TAG, "OpenCV loaded successfully");

                // Load native library after(!) OpenCV initialization
                System.loadLibrary("detection_based_tracker");

                try {
                    // load cascade file from application resources
                    InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                    File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                    File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
                    FileOutputStream os = new FileOutputStream(mCascadeFile);

                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        os.write(buffer, 0, bytesRead);
                    }
                    is.close();
                    os.close();

                    javaDetector = new JavaDetector(mCascadeFile);
                    nativeDetector = new NativeDetector(mCascadeFile);

                    currentDetector = javaDetector;

                    cascadeDir.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                }

                neuralDetector = new NeuralDetector(FdActivity.this);
                serverDetector = new ServerDetector();

                mOpenCvCameraView.enableView();
            } else {
                super.onManagerConnected(status);
            }
        }
    };

    public FdActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.face_detect_surface_view);

        fpsText = (TextView) findViewById(R.id.fps_text_id);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    protected List<? extends CameraBridgeViewBase> getCameraViewList() {
        return Collections.singletonList(mOpenCvCameraView);
    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        if (printing || currentDetector == null) return mRgba;

        Mat result = currentDetector.detect(mRgba, mGray);

        float fps = currentDetector.getFPS();

        //comm.message(fps);
        runOnUiThread(() ->  fpsText.setText(String.format(getString(R.string.fps_string), fps)));

        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
        mItemJava = menu.add("Java");
        mItemNative = menu.add("Native");
        mItemNN = menu.add("Neural Network");
        mItemNNServer = menu.add("Server");
        mItemBenchmarks = menu.add("Print Benchmark Log");
        mCurrentDetector = mItemJava;
        return true;
    }

    private void showServerDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View promptView = layoutInflater.inflate(R.layout.server_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        final EditText hostText = promptView.findViewById(R.id.host);
        final EditText portText = promptView.findViewById(R.id.port);

        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", (dialog, id) ->
                        serverDetector.startCommunication(
                                hostText.getText().toString(),
                                Integer.parseInt(portText.getText().toString())
                        )
                )
                .setNegativeButton("Cancel", (dialog, id) -> dialog.cancel());

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        if (item == mItemBenchmarks) {
            if (currentDetector.isRecording()) {
                currentDetector.stopRecording();
                printing = true;
                printBenchmark();
                printing = false;
            } else {
                currentDetector.startRecording();
            }
        } else {
            if (mCurrentDetector == mItemNNServer) {
                serverDetector.stopCommunication();
            }
            if (item != mItemNative && mCurrentDetector == mItemNative) {
                nativeDetector.stop();
            } else if (item == mItemNative && mCurrentDetector != mItemNative) {
                nativeDetector.start();
            } else if (item == mItemNNServer) {
                showServerDialog();
            }

            mCurrentDetector = item;
            if (item == mItemJava) currentDetector = javaDetector;
            else if (item == mItemNative) currentDetector = nativeDetector;
            else if (item == mItemNN) currentDetector = neuralDetector;
            else if (item == mItemNNServer) currentDetector = serverDetector;
        }
        return true;
    }

    private void printBenchmark() {
        String name = currentDetector.getName();
        StringBuilder sb = new StringBuilder(name);
        sb.append(":");
        for (int v : currentDetector.getBanchmarks()) {
            sb.append(",");
            sb.append(v);
        }
        Log.i("BENCHMARK", sb.toString());
    }

    private void setMinFaceSize(float faceSize) {
        javaDetector.mRelativeFaceSize = faceSize;
        javaDetector.mAbsoluteFaceSize = 0;
        nativeDetector.mRelativeFaceSize = faceSize;
        nativeDetector.mAbsoluteFaceSize = 0;
    }
}
