from setuptools import setup, find_packages
setup(
    name="face_detection_server",
    version="0.1",
    packages=find_packages(),

    install_requires=["opencv-python>=4.1.2", "PyYAML>=5.3"],

    package_data={
        "": ["*.prototxt", "*.caffemodel", "*.yaml"]
    },

    author="Dominik Scholz",
    author_email="dominik.scholz@tuwien.ac.at",
    description="Face Detection Server using OpenCV",
)