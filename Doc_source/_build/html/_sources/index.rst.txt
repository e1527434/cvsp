.. Face Detection Server documentation master file, created by
   sphinx-quickstart on Thu Jan 30 21:40:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Face Detection Server's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. autoclass:: face_detection_server.FaceDetectionServer
   :members:
   :special-members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
