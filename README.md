# Face Detection App

## Installation of the Face Detection App:
Transfer `.\Demo\face_detection.apk` to mobile device running on at least Android 5.0 (Lollipop, API 21), although Android version 10 (API 29) is recommended. Allow installation of apk files (should prompt automatically, otherwise in the developer settings) and install apk. During installation, permit camera access as well as network access for the app.

## User Manual for the Face Detection App:
When the app is opened, the default (Java) face detection automatically starts. The app shows the FPS of the detection in the upper left corner (landscape mode). The detections can be seen in the centered camera-view as yellow rectangles. To change the detection mode, click on the menu icon in the app bar (3 dots). Afterwards, a drop-down menu opens, where the different modi can be selected. When the server mode is selected, a prompt for the server's host and port opens. After entering these values and pressing the "Ok" button, the app tries connecting to the server.

# Face Detection Server

## Installation of the Face Detection Server:

0) (optional) create virtual environment using `python -m venv VENV_NAME` and activate by executing `VENV_NAME\Scripts\activate.bat` (Windows) or `source VENV_NAME/bin/activate` (Unix, MacOS)

1) Update pip using `python -m pip install --upgrade pip`
2) Install package `pip install .\dist\face_detection_server-0.1.tar.gz`

## Running Demo Server
A demo of the face detection server can be executed by running `python .\Demo\demo.py`.
This runs a demo server using the supplied config file located at `.\Demo\config.yaml`.

## Configuration
Objects of the `FaceDetectionServer` class can be configured by providing an optional config file (YAML format) in as the `config_file` parameter. Example config files can be seen in the demo (`.\Demo\config.yaml`). The following settings can be configured:

- **server:**
    - **port: INTEGER** _the port, on which the server will listen to incoming UDP packets_
    - **headless: BOOLEAN** _whether the server should display the camera view (false) or not (true)_
- **detection:**
    - **threshold: FLOAT** _threshold parameter for the detection_
    - **net:**
        - **prototxt: STRING** _path to the `.prototxt` file relative to the script file, uses default models from package if not found_
        - **caffemodel: STRING** _path to the `.caffemodel` file relative to the script file, uses default models from package if not found_
        - **parameter:**
            - **scalefactor: INTEGER** _scale factor used for neural net_
            - **size:**
                - **x: INTEGER** _input width_
                - **y: INTEGER** _input height_
            - **mean:**
                - **r: INTEGER** _mean red channel_
                - **g: INTEGER** _mean green channel_
                - **b: INTEGER** _mean blue channel_