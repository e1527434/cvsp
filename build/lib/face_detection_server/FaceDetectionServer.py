from pathlib import Path
import socket
import struct
import numpy as np
import cv2
import threading
import os
import yaml

class FaceDetectionServer:

    op_codes = {
        0: "UNKNOWN",
        1: "FPS",
        2: "IMAGE",
        3: "TEXT"
    }

    def __init__(self, config_path=None):
        """Constructs a server instance

        Keyword arguments:
        config_path -- path to the .yaml configuration
        """

        default_path = (Path(__file__).parent / "default.yaml")
        if not config_path:
            config_path = default_path

        with open(config_path, 'r') as ymlfile:
            cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)

        cfg_server = cfg['server']
        self.port = cfg_server['port']
        self.headless = cfg_server['headless']

        cfg_detection = cfg['detection']
        self.threshold = cfg_detection['threshold']

        net_path = config_path.parent

        cfg_net = cfg_detection['net']

        try:
            self.net = cv2.dnn.readNetFromCaffe(
                str((net_path / cfg_net['prototxt']).resolve()),
                str((net_path / cfg_net['caffemodel']).resolve())
            )
        except:
            with open(default_path, 'r') as ymlfile:
                default_cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)

            default_cfg_net = default_cfg['detection']['net']

            self.net = cv2.dnn.readNetFromCaffe(
                str((default_path.parent / default_cfg_net['prototxt']).resolve()),
                str((default_path.parent / default_cfg_net['caffemodel']).resolve())
            )

        cfg_net_parameter = cfg_net['parameter']
        self.net_parameters = {
            "scalefactor": cfg_net_parameter['scalefactor'],
            "size": (cfg_net_parameter['size']['x'], cfg_net_parameter['size']['y']),
            "mean": (cfg_net_parameter['mean']['r'], cfg_net_parameter['mean']['g'], cfg_net_parameter['mean']['b'])
        }


    def run(self):
        """Runs the server thread"""
        t = threading.currentThread()

        sock = socket.socket(socket.AF_INET, # Internet
                            socket.SOCK_DGRAM) # UDP
        sock.bind((socket.gethostname(), self.port))
        sock.settimeout(1)

        data = np.zeros(65535, dtype=np.uint8)
        rects = np.zeros(200*4, dtype=np.float)

        print("Listening to port: {}".format(self.port))
        while getattr(t, "should_run", True):
            try:
                read_length, addr = sock.recvfrom_into(data, len(data)) # buffer size is 1024 bytes
            except socket.timeout:
                continue

            op = data[0]

            if op == 1:
                fps = struct.unpack('>f', data[1:5])
            elif op == 2:
                img = cv2.imdecode(data[9:read_length+1], cv2.IMREAD_COLOR)

                if not self.headless:
                    cv2.imshow('image', img)
                    cv2.waitKey(1)

                blob = cv2.dnn.blobFromImage(img, self.net_parameters["scalefactor"], self.net_parameters["size"], self.net_parameters["mean"], True, crop=False)
                self.net.setInput(blob)
                outs = self.net.forward()
                del blob

                i = 0

                for d in outs[0][0]:
                    if d[2] > self.threshold:
                        rects[i]   = d[3] # left
                        rects[i+1] = d[4] # top
                        rects[i+2] = d[5] # right
                        rects[i+3] = d[6] # bottom
                        i += 4

                ret_data = bytearray(data[1:9]) + struct.pack('%sf' % i, *rects[:i])
                sock.sendto(ret_data, addr)
            elif op == 3:
                print("received text: {}".format(''.join([chr(c) for c in data[1:read_length+1]])))
        
        print("closing windows")
        cv2.destroyAllWindows()


    def start(self):
        """Starts the server"""
        self.thread = threading.Thread(target=self.run)
        self.thread.start()

    def stop(self):
        """Stops the server"""
        self.thread.should_run = False
        self.thread.join()